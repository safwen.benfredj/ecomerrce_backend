const mongoose = require('mongoose');
const Order = require('../models/order.models');
const Product = require('../models/product.models');

exports.getAllOrders = (req, res, next) => {
    Order
        .find()
        .populate('items.item', '_id name price quantity Category')
        .exec()
        .then(orders => {
            res.status(200).json({
                count: orders.length,
                orders: orders
            });
        })
        .catch(error => {
            next(error);
        })
};

exports.createOneOrder = async( req, res )=>{

	const order = new Order( {
		owner: req.body.owner,
		items: req.body.items,
		address: req.body.address,
		total: req.body.total
	} );
	try {
		const savedOrder = await order.save();
		const cartEmpty = await Cart.findOne( { owner: savedOrder.owner } );
		cartEmpty.emptyCart();
		return res.json( savedOrder );
	} catch ( err ) {
		return res.json( err );
	}
};
exports.getOneOrder = (req, res, next) => {
    const orderId = req.params.orderId;
    Order
        .findById(orderId)
        .populate('items.item', '_id name price quantity Category')
        .exec()
        .then(order => {
            return res.status(201).json(order);
        })
        .catch(error => {
            next(error);
        });
};

exports.updateOneOrder = (req, res, next) => {
    const orderId = req.params.orderId;
    Order
        .update({ _id: orderId }, { $set: req.body })
        .exec()
        .then(result => {
            return res.status(200).json({
                message: 'Updated Order Successfully!',
                result: result
            });
        })
        .catch(error => {
            next(error);
        });
};

exports.deleteOneOrder = (req, res, next) => {
    const orderId = req.params.orderId;
    Order
        .remove({ _id: orderId })
        .exec()
        .then(result => {
            return res.status(200).json({
                message: 'Deleted order!',
                result: result
            });
        })
        .catch(error => {
            next(error);
        });
};

function createOrder(req) {
    return new Order({
		owner: req.body.owner,
		items: req.body.items,
		address: req.body.address,
		total: req.body.total
    });
}