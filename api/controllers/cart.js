const Cart = require( "../models/cart.models" );


//Get Cart
const getCart = async( req, res )=> {

	const cart = req.cart;

	try{
		return res.json( { cart: cart } );

	}catch( err ){
		res.json( { err_message: err } );
	}
};
//update cart
const updateCart = async( req, res )=>{

	const cart = req.cart;
	try {

		const dataToUpdate = req.body;
		const { ...updateData } = dataToUpdate;
		const updateCart = await Cart.findByIdAndUpdate( cart._id, updateData, { new: true } );
		return res.json( updateCart );
	} catch ( err ) {
		res.json( err );
	}
};
//Get delete story
const deleteCart = async( req, res )=>{
	try {
		const cart = req.cart;
		const deleteCart = await Cart.findByIdAndDelete( cart._id );
		return res.json( deleteCart );
	} catch ( err ) {
		res.json( err );
	}
};

const addToCart = async( req, res )=>{

	const items = {
		item: req.body.item,
		quantity: req.body.quantity,
		price: req.body.price,
	} ;
	const currentItem=req.cart.items;
	const itemIndex=currentItem.findIndex( ( elm )=>elm.item._id==req.body.item ) ;
	if( itemIndex==-1 )
	{
		await req.cart.items.push( items );
		await req.cart.save();
		return res.json( { msg: "item added successfully" } );
	}else{
		currentItem[itemIndex].quantity+=req.body.quantity;
		const newItems=currentItem;
		await Cart.findByIdAndUpdate( req.cart._id,{ items:newItems }, { new: true } ) ;
		res.json( { msg: "item added successfully" } );
	}
}

const removeFromCart = async( req, res )=>{

	const item = req.body.item;
	const cart = req.cart;
	await Cart.findByIdAndUpdate( cart._id, { $pull: { "items": {  item: item } } }, { new : true } );
	await req.cart.save();
	return res.json( { msg: "item deleted successfully" } );
} 
module.exports.removeFromCart = removeFromCart
module.exports.addToCart = addToCart
module.exports.getCart = getCart;
module.exports.updateCart = updateCart;
module.exports.deleteCart = deleteCart;