const mongoose = require("mongoose");
const Category = require("../models/category.models");

module.exports.getAllCatogries = async (req, res) => {
  let categories = await Category.find();
  res.json({ catgories: categories });
};

module.exports.AddCategory = async (req, res) => {
  const name = req.body.name;
  let newCategory = Category({
    name: name
  });
  let result = await newCategory.save();
  res.json({
    res: result
  });
};

module.exports.updateCategory = async (req, res) => {
  const name = req.body.name;
  const id = req.params.id;
  let result = await Category.findByIdAndUpdate({ _id: id }, { name: name });
  res.json({
    res: result
  });
};

module.exports.deleCategory = async (req,res)=>{
   
    const id = req.params.id;
    let result = await Category.findByIdAndRemove({ _id: id }, { name: name });
    res.json({
      res: result
    });
}