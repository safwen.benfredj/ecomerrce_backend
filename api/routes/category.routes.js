const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth');

const CategoryController = require('../controllers/category');

router.get('/',  CategoryController.getAllCatogries);

router.post('/', checkAuth, CategoryController.AddCategory);

router.put('/:id', checkAuth, CategoryController.updateCategory);

router.delete('/:id', checkAuth, CategoryController.deleCategory);

module.exports = router;