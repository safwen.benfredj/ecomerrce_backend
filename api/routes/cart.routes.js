const router = require( "express" ).Router();
const cartControllers = require( "../controllers/cart" );
const Cart = require( "../models/cart.models" );
const User = require( "../models/user.models" );
const checkAuth = require('../middleware/check-auth');
const isCartOwner = require( "../middleware/isCartOwner" );

// this used to replace the :cart in the url with the current cart that you call by id 
router.param( "cart", async( req, res,next, id )=>{
	try{
		const cart = await Cart.findOne( { _id: id } ).populate( { path: "owner", select:"_id" } ).populate( { path: "items.item", select:[ "name", "productImage", "quantity", "category" ] } );
		if( !cart ) return res.sendStatus( 404 );
		req.cart = cart;
		return next();

	}catch( err ){
		res.json( { err_message : err } );
	}
} );
// this used to replace the :owner in the url with the current user that you call by id 
router.param( "owner", async( req, res,next, id )=>{
	try{
		const owner = await User.findOne( { _id: id } );
		if( !owner ) return res.sendStatus( 404 );
		req.owner = owner;
		return next();

	}catch( err ){
		res.json( { err_message : err } );
	}
} );


router.put( "/:owner/:cart/additem",checkAuth ,isCartOwner, cartControllers.addToCart );
router.put( "/:owner/:cart/removeitem",checkAuth ,isCartOwner, cartControllers.removeFromCart);

router.get( "/:owner/:cart",checkAuth,isCartOwner, cartControllers.getCart );

router.put( "/:owner/:cart/update" ,checkAuth,isCartOwner, cartControllers.updateCart );
router.put( "/:owner/:cart/delete" ,checkAuth,isCartOwner, cartControllers.deleteCart );
module.exports = router;
