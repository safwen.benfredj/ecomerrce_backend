const mongoose = require("mongoose");

const productSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: { type: String, required: true },
  price: { type: Number, required: true },
  quantity :{type: Number, require: true},
  Category: { type: mongoose.Schema.Types.ObjectId, ref: "Category" },
  productImage: { type: String }
});

module.exports = mongoose.model("Product", productSchema)|| mongoose.models.Product;
