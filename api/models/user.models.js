const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    firstName: { type: String, max: 64 },
	lastName: { type:String, max: 64 },
    email: {
        type: String,
        required: true,
        unique: true,
        match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    },
    password: { type: String, required: [ true, "can't be blank" ], max: 1024 },
	phoneNumber: { type: Number },
    address: { type: String },
    cart: { type: mongoose.Schema.Types.ObjectId, ref: "Cart" }
});

UserSchema.plugin( uniqueValidator, { message: "is already taken." } );

module.exports = mongoose.model('User', userSchema)|| mongoose.models.User