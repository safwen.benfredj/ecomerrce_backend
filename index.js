const path = require( "path" );
const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const dotenv = require("dotenv");

const productRoutes = require("./api/routes/products.routes");
const orderRoutes = require("./api/routes/orders.routes");
const userRoutes = require("./api/routes/user.routes");
const categoryRoutes = require("./api/routes/category.routes");
const cartRoutes = require("./api/routes/cart.routes");

dotenv.config();

mongoose.set( "useNewUrlParser", true );
mongoose.set( "useFindAndModify", false );
mongoose.set( "useCreateIndex", true );
mongoose.set( "useUnifiedTopology", true );
mongoose.connect(process.env.DATABASE_STRING).then(data => {
  console.log("Connected....");
});

const app = express();
// Log request data
app.use(morgan("dev"));

// Setup static files path
app.use( "/uploads", express.static( path.join( __dirname + "/uploads" ) ) );
// Use body parser middleware to parse body of incoming requests
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Setup CORS
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});

// Routes which should handle requests
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
app.use("/user", userRoutes);
app.use("/category", categoryRoutes);
app.use('/cart', cartRoutes)
// Handle Error Requests
app.use((req, res, next) => {
  const error = new Error();
  error.message = "Not Found";
  error.status = 404;
  next(error);
});
//test

app.use((error, req, res, next) => {
  res.status(error.status || 500).json({
    error: error
  });
});
const port = process.env.PORT || 3000;

app.listen(3000, () => {
  console.log("app is running..");
});
